﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeHommik
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Juhuarvu õpetus
            ////Juhuslikud arvud

            //Random r = new Random(); //arvuti kella järgi
            ////Random r = new Random(3453535); initial vector'i järgi
            //int i = r.Next();
            //Console.WriteLine(i);

            //i = r.Next(100);
            //Console.WriteLine(i);

            //i = r.Next(10, 20);
            //Console.WriteLine(i);

            //double d = r.NextDouble();
            //Console.WriteLine(d); 
            #endregion

            int[] juhuMassiiv = new int[100];
            Random r = new Random();
            
            for (int i = 0; i < 100; i++)
            {
                juhuMassiiv[i] = r.Next(0,100);
            }


            for (int i = 0; i < 100; i++)
            {
                if (i % 10 == 9) Console.WriteLine((juhuMassiiv[i] < 10 ? " " : "")+juhuMassiiv[i] + "  ");
                if (i % 10 != 9) Console.Write((juhuMassiiv[i] < 10 ? " " : "") + juhuMassiiv[i] + "  ");               
            }

            int suurim = juhuMassiiv[0];
            int vähim = juhuMassiiv[0];
            double summa = 0; //double, sest muidu ümardab arvude summa ja koguse jagatise

            for (int i = 1; i < juhuMassiiv.Length; i++)
            {
                suurim = juhuMassiiv[i] > suurim ? juhuMassiiv[i] : suurim;
                vähim = juhuMassiiv[i] < vähim ? juhuMassiiv[i] : vähim;
                summa += juhuMassiiv[i];
            }

            Console.WriteLine("Suurim on " + suurim);
            Console.WriteLine("Vähim on " + vähim);
            Console.WriteLine("Keskmine on " + (summa / juhuMassiiv.Length));

        }
    }
}
