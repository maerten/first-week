﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            //Massiiv
            int[] arvud = new int[7] { 1, 5, 3, 8, 9, 2, 3 };
            int[] arvudx = { 1, 5, 3, 8, 9, 2, 3 }; //kui on ühes lauses
            arvud = new int[] { 1, 5, 3, 8, 9, 2, 3 }; //saab ka nii
            //foreach on readonly - ei saa muuta. Samuti ei saa pööruda konkreetse elemendi poole 
            foreach (var arv in arvud) Console.WriteLine(arv);
            //Massiivi puudus: suurus on igavene - ei saa suuremaks ega väiksemaks

            //List
            List<int> arvud2 = new List<int> { 1, 5, 3, 8, 9, 2, 3 };
            foreach (int arv in arvud2) Console.WriteLine(arv);
            arvud2[3]++;//Lisab kolmandale juurde 1

            arvud2.Add(17);
            arvud2.Remove(8);
            arvud2.RemoveAt(2);

            foreach (var arv in arvud2) Console.WriteLine(arv);

            //kui korjata ära kõik kaheksed
            while (arvud2.Contains(8)) arvud2.Remove(8);

            List<int> arvud3 = new List<int>(); //NB! Sulud on lõpus! Võib olla ka loogelised sulud { }
            //List<int> arvud3 = new List<int>(100); //Võib kohe suuruse ette anda
            arvud3.Add(8);
            arvud3.Add(3);
            arvud3.Add(4);
            arvud3.Add(5);
            arvud3.Add(6);

            //Listil ei ole length'i - on Count
            //for (int i = 0; i < arvud2.Count; i++)

            //Kui list saab täis, läheb ta kohe 2x suuremaks
            for (int i = 0; i < 20; i++)
            {
                arvud3.Add(i * i);
                //Console.WriteLine("maht: " + arvud3.Capacity + " suurus: " + arvud3.Count);
                Console.WriteLine($"maht: { arvud3.Capacity} suurus: { arvud3.Count}");
            }

            //Massiivi ja listi vaheline teisendus
            arvud3 = arvud.ToList();
            arvud = arvud3.ToArray();

            //List<string> nimekiri

            //Massiiv - Array
            //T[] muutuja = new T[suurus];
            //List<T> muutuja = new List<T>();

            //Massiivil muutuja[index] Length
            // Listil muutuja[index] Count Capacity Add Remove RemoveAt

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 } }; //Length=6 - mitmest elemendist koosneb                                                       
            //GetLength(0)=2
            //GetLength(1)=3
            int[][] suur = new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6, 7 } }; //Length - mitmest massiivist massiiv koosneb
            //suur.Length=2 mitu massiivi
            //suur.Length[0]=3 mitu liiget esimeses massiivis
            //suur.Length[1]=4 mitu teises

            //listideList.Add(new List<int> { 1, 2, 3 });
            //List<List<T>>

            //Veel listutaolisi asju
            //Queue(järjekord) - Enque(pane järjekorra lõppu), Deque(võta järjekorrast esimene)
            //Stack(kuhi) - Push(pane juurde), Pop(võta pealt ära)

            //Dictionary
            Dictionary<int, string> nimekiri = new Dictionary<int, string> //<võti, väärtus> <key, value>
            {
                {1,"Henn"},
                { 2,"Ants"},
                { 3,"Peeter"}
            };


            //nimekiri.Add(7,"Joosep");
            //nimekiri[2] = "Antsuke";
            //nimekiri[9] = "Pilleke";

            Dictionary<string, double> Tulemused = new Dictionary<string, double>
            {
                {"Hiir", 1.111 },
                {"Jänes",200/6.0 }
            };

            Console.WriteLine(Tulemused["Jänes"]);

            foreach (var k in Tulemused.Keys.ToList()) //Kõigepealt korjame võtmed "taskusse" ja siis käime need läbi
            {
                Tulemused[k] *= 2; //Tulemused 2x paremaks
            }


            //Contains - array, list, stack, queue

            //Dictionary
            //.ContainsKey
            //.ContainsValue

            //Dictionary'l 3 foreach'i

            //foreach(var x in Dictionary) -  annab paarid
            //foreach(var x in Dictionary.Keys) -  annab võtmed
            //foreach(var x in Dictionary.Values) -  annab väärtused

            //SortedDictionary<TKey,TValue> - võtmete järjekorras, nagu Dict aga järjekorras
            //SortedListTKey,TValue> nagu Dict aga järjekorras
            //SortedSet<T> - listi pandud asjad järjekorras, nagu list aga sorteeritud järjekorras
            
            //Tuleb valide selline, mis on jõudluse mõttes parem (vt StackOverflow tabel)
        }
    }
}
