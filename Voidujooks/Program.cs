﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Voidujooks
{
    class Program
    {
        static void Main(string[] args)
        {
            //Märteni algus ja Hennu koodi lõpp
            string failinimi = @"..\..\spordipäeva protokoll.txt";
            string[] nimekiri = File.ReadAllLines(failinimi);

            string[] nimed = new string[nimekiri.Length - 1];
            double[] kiirused = new double[nimekiri.Length - 1];

            int kiireim = 0;

            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = nimekiri[i + 1].Replace(", ", ",").Split(',');
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}");
                if (kiirused[i] > kiirused[kiireim]) kiireim = i;
            }

            Console.WriteLine($"Kõige kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");


            ////Hennu kood
            //string filename = "..\\..\\spordipäeva protokoll.txt";
            //// Console.WriteLine(File.ReadAllText(filename)); // kontrollin kas saab lugeda - hiljem kustutan

            //string[] read = File.ReadAllLines(filename); // tulemuseks massiivi ridadega asi

            //string[] nimed = new string[read.Length - 1]; // mis kuradi - 1?
            //double[] kiirused = new double[read.Length - 1];

            //int kiireim = 0;
            //for (int i = 0; i < nimed.Length; i++)
            //{
            //    string[] tykid = read[i + 1].Replace(", ", ",").Split(','); // kaval - ma polegi nii varem teinud
            //    nimed[i] = tykid[0];
            //    kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
            //    Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}"); // hää oli vahepeal vaadata, kas sain hakkama
            //    if (kiirused[i] > kiirused[kiireim]) kiireim = i;
            //}

            //Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");

        }
    }
}
