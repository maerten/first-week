﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeastArvutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            int punkte = 50;
            Random r = new Random();
            
            for (int i = 0; i < 10; i++)
            {              
                int arv1 = r.Next(1,10);
                int arv2 = r.Next(1,10);
                string[] margid = { "+", "-", "*", "/" };
                string mark = margid[r.Next(4)];

                string avaldis = arv1 + mark + arv2;
                Console.Write("Kui palju on " + avaldis + "? ");
                int tulemus=0;

                switch (mark)
                {
                    case "+":
                        tulemus = arv1 + arv2;
                        break;
                    case "-":
                        tulemus = arv1 - arv2;
                        break;
                    case "*":
                        tulemus = arv1 * arv2;
                        break;
                    case "/":
                        tulemus = arv1 / arv2;
                        break;
                }

                //Märteni lahendus
                for (int j = 0; j < 5; j++)
                {
                    int vastus = int.Parse(Console.ReadLine());
                    if (vastus == tulemus)
                    {
                        Console.WriteLine("Õige vastus!");
                        break;
                    }
                    else
                    {
                        punkte--;
                        if (j < 4) Console.Write("Proovi uuesti! ");
                        if (j == 4) Console.WriteLine("Ei saanudki õiget vastust");
                    }

                }

                //// Hennu lahendus (muudetud)
                //for (int j = 0; j < 5; j++)
                //{
                //    if (int.Parse(Console.ReadLine()) == tulemus)
                //    {
                //        Console.WriteLine("Õige vastus!");
                //        break;
                //    }
                //    Console.WriteLine($"vale vastus{(j==4 ? " - ei saanudki õiget\r\n" : " - proovi uuesti: ")}");
                //    punkte--;
                //}
            }
            Console.WriteLine("Said " + punkte + " punkti");

            Console.WriteLine("See tähendab, et hinne on " +(punkte < 40 ? "4" :
                                                            punkte < 30 ? "3" :
                                                            punkte < 20 ? "2" :
                                                            punkte < 10 ? "1" : "5"));
        }
    }
}
