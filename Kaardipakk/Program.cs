﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaardipakk
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();

            int[] kaardipakk = new int[52]; //tühi paki massiiv
            Console.Write((kaardipakk[0] = r.Next(1, 53))+ "\t"); //[0]-element paigas       
            int juhuarv;
            bool kaartValitud;
            int k = 0;

            for (int i = 1; i < 52; i++) //kaardipaki positsioonide täitmine alates [1]-elemendist
            {
                kaartValitud = false; //esialgu ei ole valitud
                do //teeb kuni sobiv kaart valitud
                {
                    //k++; //loendur
                    juhuarv = r.Next(1, 53);                   

                    for (int j = 0; j < i; j++) //kontrollib, kas juhuarv on juba valitud pakis
                    {
                        //k++; //loendur
                        if (kaardipakk[j] == juhuarv) break; //kui on, siis väljuv for-st ja do järgmine juhuarv
                        if (j == (i - 1)) kaartValitud = true; //do-st väljumise tingimus
                    }

                } while (!kaartValitud);
                
                kaardipakk[i] = juhuarv; //kui saab do-st välja, siis saab juhuarvust järmine valitud kaart

                if (i % 13 == 0) Console.Write("\n");
                Console.Write(kaardipakk[i]+ "\t");
            }
        }
    }
}
