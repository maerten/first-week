﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud; //ilma sisuta massiiv
            arvud = new int[10]; //koosneb 10-st int'ist

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i; //indeks[i]
            }

            foreach (var x in arvud) Console.WriteLine(); // x on read only - muuta ei saa

            ////Identne eelmise reaga
            //for (var e = arvud.GetEnumerator(); e.MoveNext();)
            //{
            //    var x = e.Current;
            //    Console.WriteLine(x);
            //}

            int[] teisedarvud = new int[10];
            int[] kolmandadaarvud = teisedarvud; //saavad üheks ja samaks massiiviks
            //int[] kolmandadaarvud = (int[]teisedarvud.Clone(); //nüüd on kaks massiivi
            kolmandadaarvud[3] = 7;
            Console.WriteLine(teisedarvud[3]); //vastus on 7

            int[] veelYks = new int[5] { 1, 3, 6, 2, 7 }; //täidetakse
            int[] viimane = { 1, 2, 3, 4, 5, 6 }; //ei pea panema initializer new

            int[][] paljuarve; //massiivide massiiv

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; //kahemõõtmeline massiiv
        }
    }
}
