﻿using System;

namespace TeineKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            #region MyRegion
            //Console.WriteLine("a=");
            //int a = int.Parse(Console.ReadLine());
            //Console.WriteLine("b=");
            //int b = int.Parse(Console.ReadLine());
            //int c = a + b;
            //Console.WriteLine("c=" + c);

            //avaldised koosneb:
            //literaalid - arvud ja muud konstandid
            //muutujad
            //tehted
            //sulud
            //funktsioonid

            //Console.WriteLine(3>2 ? "suurem" : "väiksem");

            //unaarne
            //binaarne
            //ternaarne

            // =omistamine
            // ==võrdlemine
            // ===samasustehe (et ka tüüp oleks sama)

            //int arv = 20;
            //Console.WriteLine(arv++); //20 (vana=20, uus=21) (trükib vana väärtuse enne 1 juurde liitmist)
            //Console.WriteLine(++arv); //22 (vana=21, uus=22) (trükib uue väärtuse pärast 1 juurde liitmist)

            //string tekst = @"Users/maerten"; // @-märk teeb non-escape
            //string tekst2 = "luts jutustab \"kui arno isaga\""; //eskeipimata
            //string tekst3 = @"luts jutustab ""kui arno isaga"""; //eskeipimata string, siis topelt jutumärgid

            //int arv1 = 7;
            //int arv2 = 10;

            ////placeholderid stringis (stringFormat)
            //Console.WriteLine("arvud on {0} ja {1}", arv1, arv2);

            //string tulemus = string.Format($"arvud on {arv1} ja {arv2}");
            //Console.WriteLine(tulemus);

            ////placeholder avaldised - interpoleeritud string
            //Console.WriteLine($"arvud on {arv1} ja {arv2}");

            //tulemus = $"arvud on {arv1} ja {arv2}, vaatame, kas töötab";
            //Console.WriteLine(tulemus);
            #endregion

            //var muutuja = 7; //ei anna tüüpi, kuid määrab ise, et on  muutuja int 
            //Console.WriteLine(muutuja.GetType().Name);
            //var teine = muutuja; //mõlemad sama tüüpi (int) 

            ////rename - parema klahviga menüüst Rename või Ctrl+R, Ctrl+R

            ////Teisendamine
            //if= 1000;
            //int l = int; //toimub ilmutamata tüübiteisendus (väiksemast suuremasse tüüpi)
            //int= (int)l; //ilmutatud tüübiteisendus e casting (siis kui suuremast väiksemasse tüüpi)

            ////Tehete prioriteedid

            ////Negatiivne kahendnumbriga = vastupidised märgid + 1
            ////0000 0101 +5
            ////1111 1011 -5

            //decimal dets = int;
            //decimal= (float)dets;
            
            //string tekst = "100";
            ////i=(int)tekst; //ei toimi - stringi ei saa castida teisteks tüüpideks
            ////tekst=d //ka ei toimi

            //tekst = d.ToString.();

            Console.Write("Millal sündinud? ");
            string aeg = Console.ReadLine();
            //DateTime päev = DateTime.Parse(aeg);
            //Console.WriteLine($"Sünnipäev on päeval {päev.DayOfWeek}");

            //TryParse - proovib, kas õnnestub parsida
            if(DateTime.TryParse(aeg, out DateTime sündinud))
            {
                Console.WriteLine("IF-lausega sinu sünna on siin " + sündinud.ToShortDateString());
            }
            else
            {
                Console.WriteLine("IF: see pole sünna");
            }

            //Elvisega
            Console.WriteLine(
                DateTime.TryParse(aeg, out DateTime x) ? $"Elvisega sa oled sündinud {x})" : "Elvis: see pole kuupäev"
                );

        }
    }
}


