﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaheMootmelineMassiiv
{
    class Program
    {
        static void Main(string[] args)
        {

            //Kahemõõtmeline massiiv 10x10 juhuarvudega kuni 100
            Random r = new Random();
            int[,] tabel = new int[10,10];

            for (int i = 0; i < tabel.GetLength(0); i++) //GetLength(0) - esimese mõõtme suurus
            {                
                for (int j = 0; j < tabel.GetLength(1); j++) //GetLength(1) - teise mõõtme suurus
                {
                    tabel[i, j] = r.Next(0, 100);
                    Console.Write((tabel[i,j] < 10 ? " " : "") + tabel[i,j] + "  ");
                }
                Console.WriteLine("\r");
            }

            //Arvu otsimine tabelist
            Console.WriteLine("Mis arvu otsime?");
            
            
            int arv = int.Parse(Console.ReadLine());

            for (int i = 0; i < tabel.GetLength(0); i++)
            {
                for (int j = 0; j < tabel.GetLength(1); j++)
                {
                    if (arv == tabel[i, j])
                    {
                        Console.WriteLine("Arv asub tabelis real nr " + (i + 1) + " veerus nr " + (j + 1));
                    }
                }
            }



        }
    }
}
