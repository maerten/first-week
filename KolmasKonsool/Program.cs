﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LausePlokid
{
    class Program
    {
        static void Main(string[] args)
        {
            #region IF-laused
            //if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            //{
            //    Console.WriteLine("Täna sauna!");
            //}
            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            //{
            //    Console.WriteLine("Täna pühapäev");
            //}
            //else
            //{
            //    Console.WriteLine("Tööpäevad");
            //} 
            #endregion

            #region Nimereeglid
            //NIMEREEGILD
            // 1. Nimi on üks sõna
            // 2. Koosneb tähtedest, numbritest ja erandina _
            // 3. Ei tohi alata numbriga
            // 4. Ei tohi olla võtmesõna
            //int i = 7 ei tohi
            //int @if =7 tohib

            //NIME SOOVITUSED
            // 1. Nimi tähendusega
            // 2. Inglise keeles (või eesti)
            // 3. Mitmesõnalised nimed kirjutatakseNiiMoodi
            // Klassinimed suure AlgusTähega
            // võtmesõnad üleni väikeste tähtedega
            // kohalikud muutuja nimed väikese algustähega (
            // kaugele paistvad muutujad suure algustähega
            // const int SUURTE_TÄHTEDEGA=5
            // _ - see on ka muutuja nimi (mul on tast suva, ei huvita)
            // 4. Ühetähelised muutujad kasutada väga lühikestes plokkides, silmaga haaratavas, ise kirjeldavas 
            #endregion


            #region Switch
            //switch (DateTime.Now.DayOfWeek)
            //{
            //    case DayOfWeek.Saturday:
            //        Console.WriteLine("Laupäev");
            //        break;
            //        // Võib kirjutada goto case .....
            //    case DayOfWeek.Sunday:
            //        Console.WriteLine("Pühapäev");
            //        break;
            //    default:
            //        Console.WriteLine("Muud päevad");
            //        break; 
            #endregion


            Console.Write("Mis värvi tuli põleb fooris? ");
            string foorColor = Console.ReadLine();

            Console.Write("Kas teeme IF-iga või Swich-iga? ");
            string ifOrSwitch = Console.ReadLine();

            string lause;

            // switch (Console.ReadKey().KeyChar) - siis ei pea tegema stringi
            // case 'i': case 'I':

            switch (ifOrSwitch)
            {
                case "if":
                    //// if-laused plokkidena
                    //if (foorColor == "punane")
                    //{
                    //    Console.WriteLine("Stop");
                    //}
                    //else if (foorColor == "kollane")
                    //{
                    //    Console.WriteLine("Oota veidi!");
                    //}
                    //else if (foorColor == "roheline")
                    //{
                    //    Console.WriteLine("Võid minna üle tee");
                    //}
                    //else
                    //{
                    //    Console.WriteLine("Mis jama selle fooriga on?!");
                    //}
                    //Console.WriteLine("IF-iga tuli hästi välja!");
                    //break;

                    // if-laused ühe roduna
                    if (foorColor == "punane") lause = "Stop";
                    else if (foorColor == "kollane") lause = "Oota veidi!";
                    else if (foorColor == "roheline") lause = "Võid minna üle tee";
                    else lause = "Mis jama selle fooriga on?!";
                    
                    Console.WriteLine(lause);
                    Console.WriteLine("IF-iga tuli hästi välja!");
                    break;

                case "switch":
                    switch (foorColor)
                    {
                        case "punane":
                            Console.WriteLine("Stop");
                            break;
                        case "kollane":
                            Console.WriteLine("Oota veidi");
                            break;
                        case "roheline":
                            Console.WriteLine("Võid minna üle tee");
                            break;
                        default:
                            Console.WriteLine("mis jama selle fooriga on?!");
                            break;
                    }
                    Console.WriteLine("Switchiga tuli hästi välja!");
                    break;
                default:
                    Console.WriteLine("Sellist võimalust pole!");
                    break;
            }






        }
        }
    }