﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsyklid
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 0; i < 10; i++)
            //{
            //    Console.WriteLine($"Teen seda {i+1}- korda");
            //}

            //Tühja plokiga tsükkel
            //int vastus = 0;
            //for (
            //    Console.WriteLine("Mis su palk on?");
            //    ! (int.TryParse(Console.ReadLine(), out vastus));
            //    Console.WriteLine("Kirjuta ilusti")
            //    )
            //{ }
            //Console.WriteLine($"Su palk on {vastus}");

            ////while-tsükkel
            //while (DateTime.Now.DayOfWeek !=DayOfWeek.Friday)
            ////for (; DateTime.Now.DayOfWeek !=DayOfWeek.Friday ;) - teeb sama välja
            //{
            //    Console.WriteLine("Ootan reedet");
            //    System.Threading.Thread.Sleep(1000);
            //}

            ////do-tsükkel
            //do
            //{
            //    Console.WriteLine("Ootan reedet");
            //} while (true);



            string lause;
            string tuli;

            ////Valgusfoor if'iga
            //do
            //{
            //    Console.WriteLine("Mis tuli põleb?");
            //    tuli = Console.ReadLine();
            //    if (tuli == "punane") lause = "Stop";
            //    else if (tuli == "kollane") lause = "Oota";
            //    else if (tuli == "roheline") lause = "Võid sõita";
            //    else lause = "Foor ajab jama";
            //    Console.WriteLine(lause);
            //} while (tuli != "roheline");

            
            //Sama asi switch'iga
            do
            {
                Console.WriteLine("Mis tuli põleb?");
                tuli = Console.ReadLine();
                switch (tuli)
                {
                    case "punane":
                        lause = "Stop";
                        break;
                    case "kollane":
                        lause = "Oota";
                        break;
                    case "roheline":
                        lause = "Sõida";
                        break;
                    default:
                        lause = "Foor ajab jama";
                        break;
                }
                Console.WriteLine(lause);
            } while (tuli != "roheline");
            

        }
    }
}
